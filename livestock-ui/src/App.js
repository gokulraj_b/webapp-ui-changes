import Navbar from './components/Navbar';
import Footer from './components/Pages/Home/Footer';
import {BrowserRouter as Router,Switch, Route} from 'react-router-dom';
import './App.css';
import Buy from './components/Pages/ProductBuy/Buy';
import ProductDetail from './components/Pages/ProductBuy/ProductDetail'
import Sell from './components/Pages/ProductSell/Sell';
import SellerProdct from './components/Pages/SellerProducts/SellerProductsPage'
import Cart from './components/Pages/UserCart/CartPage';
import Home from './components/Pages/Home/Home';
import AboutUs from './components/Pages/Site-details/AboutUs';
import Howitworks from './components/Pages/Site-details/How-it-works';
import Careers from './components/Pages/Site-details/Careers';
import Support from './components/Pages/Site-details/Support';
import UserProfileDetail from './components/Pages/User/UserProfile'
import UserOrders from './components/Pages/UserOrder/OrderPage';
import PlaceOrder from './components/Pages/UserOrder/PlaceOrder'
import UserAddresses from './components/Pages/User/UserAddress'
import Userlogout from './components/Pages/User/UserLogout'
import Services from './components/Pages/UnderConstruction'

function App() {

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/buy/:typeId' exact component={Buy} />
        <Route path='/product/:productId' exact component={ProductDetail} />
        <Route path='/sell' exact component={Sell} />
        <Route path='/seller-product/' exact component={SellerProdct} />
        <Route path='/userprofile/:userId' exact component={UserProfileDetail} />
        <Route path='/my-cart' exact component={Cart} />
        <Route path='/confirm-order/:addressId' exact component={PlaceOrder} />
        <Route path='/orders' exact component={UserOrders} />
        <Route path='/my-addresses/:type/:orderId' exact component={UserAddresses} />
        <Route path='/aboutus' exact component={AboutUs} />
        <Route path='/how-it-works' exact component={Howitworks} />
        <Route path='/careers' exact component={Careers} />
        <Route path='/support' exact component={Support} />
        <Route path='/logout' exact component={Userlogout} />
        <Route path='/services' exact component={Services} />
      </Switch>
      <Footer />
    </Router>
          
  );
}

export default App;
