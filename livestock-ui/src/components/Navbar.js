import React, { useEffect, useState } from 'react';
import Dropdown from 'react-bootstrap/Dropdown'
import SignInModal from './Pages/User/SignInModal';
import { FaShoppingCart, FaUser , FaBars, FaWindowClose, FaBuyNLarge} from "react-icons/fa";
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import { useHistory } from "react-router-dom";

import configData from "../config.json";
import './Navbar.css';
import Nav from 'react-bootstrap/Nav'

export default function Navbar() {

    const [click, setClick] = useState(false);
    const [dropdown, setDropdown] = useState(false);

    const history = useHistory();
       
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
    const handleDropdown = () => setDropdown(!dropdown);

    const manageAddress = 'manage/invalid'

    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };

    const handleShow = () => setShow(true);

    const handleManageAddressOnClick = (e) => {
        console.log(e)
        e.currentTarget.href = e.currentTarget.href + manageAddress
    }
    
    const onMouseEnter = () => {
        if (window.innerwidth < 960) {
            setDropdown(false);
        }
        else {
            setDropdown(true);
        }
    };
    const onMouseLeave = () => {
        if (window.innerWidth < 960) {
            setDropdown(false);
        }
        else {
            setDropdown(false);
        }
    }
    const openNav = ()=> {
        document.getElementById("mySidenav").style.width = "250px";
      }
      
      const closeNav= ()=>{
        document.getElementById("mySidenav").style.width = "0";
      }
    return (
        <div className="navbar">
            <a onClick={openNav} className="nav-link-bar"> <FaBars /> </a>
            <div className="navbar-logo">
                <a href="/" className="nav-site-title">FarmAnimalia</a>
            </div>
            
            <div className="nav-menu-div">
                <Nav defaultActiveKey="/home" as="ul" className="nav-menu">
                    <Nav.Item as="li">
                        <a href="/" className="nav-links">Home</a>
                    </Nav.Item>
                    <Nav.Item as="li">
                        <a href="/buy/0" className="nav-links">Buy</a>
                    </Nav.Item>
                    {Cookies.get('name') && Cookies.get('type') != null
                        && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 1
                        && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 5 ?
                        <Nav.Item as="li">
                            <a href="/sell" className="nav-links"> Sell </a>
                        </Nav.Item>
                        :
                        null}
                    <Nav.Item as="li">
                        <a href="/my-cart" className="nav-links"> <FaShoppingCart /> </a>
                    </Nav.Item>
                    <Nav.Item as="li">
                        <Dropdown>
                            <a href="#" className="nav-links">
                                {/* My Account */}
            
                            </a>
                            {Cookies.get('name') != null ?
                                <Dropdown.Toggle variant="success" id="collasible-nav-dropdown"><FaUser /> Welcome {CryptoJS.AES.decrypt(Cookies.get('name'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)}</Dropdown.Toggle>
                                :
                                <Dropdown.Toggle variant="success" id="collasible-nav-dropdown" />
                            }
        
                            {/* id="dropdown-split-basic" */}
                            <Dropdown.Menu>
                                {Cookies.get('name') != null ?
                                    <div>
                                        <Dropdown.Item href={`/userprofile/${CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)}`}>My Profile</Dropdown.Item>
                                    </div>
                                    :
                                    <div>
                                        <Dropdown.Item href="#" onClick={handleShow}><FaUser /> Login/register</Dropdown.Item>
                                        <SignInModal
                                            show={show}
                                            setModalClose={handleClose}
                                            onHide={handleClose}
                                        />
                                    </div>
                                }
                                <Dropdown.Divider />
                                {Cookies.get('type') != null
                                    && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 1
                                    && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 5 ?
                                    <div>
                                        <Dropdown.Item href="/seller-product/" >View Listed Products</Dropdown.Item>
                                    </div>
                                    :
                                    <Dropdown.Item href="/orders">My Orders</Dropdown.Item>
                                }
                                {Cookies.get('type') != null
                                    && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) == 9 ?
                                    <Dropdown.Item href="/orders">View Orders</Dropdown.Item>
                                    : null
                                }
                                <Dropdown.Divider />
                                {Cookies.get('name') != null ?
                                    <div>
                                        <Dropdown.Item href="/my-addresses/" onClick={handleManageAddressOnClick}>Manage Address</Dropdown.Item>
                                        <Dropdown.Item href="/logout">SignOut</Dropdown.Item>
                                    </div>
                                    :
                                    null
                                }
                            </Dropdown.Menu>
                        </Dropdown>
                    </Nav.Item>
                </Nav>
            </div>
            <div id="mySidenav" className="sidenav">
            <Nav.Item as="li">
            <a className="closebtn" onClick={closeNav}>< FaWindowClose /></a>
            </Nav.Item>
                    <Dropdown>
                            <a href="#" className="nav-links">
                                {/* My Account */}
            
                            </a>
                            {Cookies.get('name') != null ?
                                <Dropdown.Toggle variant="success" className="dropdowncls" id="collasible-nav-dropdown"><FaUser /> Welcome {CryptoJS.AES.decrypt(Cookies.get('name'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)}</Dropdown.Toggle>
                                :
                                <Dropdown.Toggle className="dropdowncls2" variant="success" id="collasible-nav-dropdown" ><FaUser /> My Account </Dropdown.Toggle>
                            }
        
                            {/* id="dropdown-split-basic" */}
                        <div className="dropdowncontent"> 
                            <Dropdown.Menu>
                                {Cookies.get('name') != null ?
                                    <div>
                                        <Dropdown.Item href={`/userprofile/${CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)}`}>My Profile</Dropdown.Item>
                                    </div>
                                    :
                                    <div>
                                        <Dropdown.Item href="#" onClick={handleShow}><FaUser /> Login/register</Dropdown.Item>
                                        <SignInModal
                                            show={show}
                                            setModalClose={handleClose}
                                            onHide={handleClose}
                                        />
                                    </div>
                                }
                                <Dropdown.Divider />
                                {Cookies.get('type') != null
                                    && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 1
                                    && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 5 ?
                                    <div>
                                        <Dropdown.Item href="/seller-product/" >View Listed Products</Dropdown.Item>
                                    </div>
                                    :
                                    <Dropdown.Item href="/orders">My Orders</Dropdown.Item>
                                }
                                {Cookies.get('type') != null
                                    && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) == 9 ?
                                    <Dropdown.Item href="/orders">View Orders</Dropdown.Item>
                                    : null
                                }
                                <Dropdown.Divider />
                                {Cookies.get('name') != null ?
                                    <div>
                                        <Dropdown.Item href="/my-addresses/" onClick={handleManageAddressOnClick}>Manage Address</Dropdown.Item>
                                        <Dropdown.Item href="/logout">SignOut</Dropdown.Item>
                                    </div>
                                    :
                                    null
                                }
                            </Dropdown.Menu>
                        </div>
                    </Dropdown>
                    <Nav.Item as="li">
                                <a href="/" className="nav-links">Home</a>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <a href="/buy/0" className="nav-links">Buy</a>
                            </Nav.Item>
                            {Cookies.get('name') && Cookies.get('type') != null
                                && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 1
                                && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 5 ?
                                <Nav.Item as="li">
                                    <a href="/sell" className="nav-links"> Sell </a>
                                </Nav.Item>
                                :
                                null}
                            <Nav.Item as="li">
                                <a href="/my-cart" className="nav-links"> <FaShoppingCart /> My Cart</a>
                            </Nav.Item>
                            <Nav.Item as="li">
                    </Nav.Item>
            </div>
            <div>
            <Nav.Item as="li">
                        
                        <a href="/" className="nav-site-title2">FarmAnimalia</a>
            </Nav.Item>
            </div>
        </div>
    );
}