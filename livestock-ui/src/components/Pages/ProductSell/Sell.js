import React, { useEffect, useState } from 'react'
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import { useHistory, Link } from "react-router-dom";

import configData from '../../../config.json'
import SignInModal from '../User/SignInModal';
import SellDetails from './SellDetails';
import { Button } from 'react-bootstrap';
 
export default function Sell() {
        
    const history = useHistory();
    const [userAddress, setUserAddress] = useState([])

    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };

    useEffect(() => {

        if (Cookies.get('id') != null && Cookies.get('type') != null
            && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 1
            && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 5) {
            fetch(`/api/v1/user-address/user=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    if (!resp || (resp && resp.length == 0)) {
                        history.push('/my-addresses/sell/selleraddress')
                    }
                    setUserAddress(resp)
                })
                .catch((err) => {
                    console.log(err);
                    history.push('/my-addresses/sell/selleraddress')
                })
        }
    }, [])


    return (
        <div>
            {Cookies.get('id') != null ?
                <div>

                    {Cookies.get('type') != null
                        && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 1
                        && CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) != 5 ?
                        <div>
                            <SellDetails pickupaddress={userAddress} />
                        </div>
                        :
                        <div style={{
                            display: 'flex', flexDirection: 'column',
                            justifyContent: 'center'
                        }} className="sell-user-login-request">
    
                            <div className="sell-user-login-request-header">
                                Only Sellers can register their products here.
                            </div>
                        </div>
                    }
                </div>
                :
                <div style={{
                    display: 'flex', flexDirection: 'column',
                    justifyContent: 'center'
                }} className="sell-user-login-request">

                    <div className="sell-user-login-request-header">
                        You have not logged in yet!
                    </div>
                    <div className="sell-user-login-request-header-link-div">
                        <Button className="sell-user-login-request-header-link" onClick={handleShow}>Signin here to register your products</Button>
                    </div>
                    <div>
                        <SignInModal
                            show={show}
                            setModalClose={handleClose}
                            onHide={handleClose}
                        />
                    </div>
                </div>
            }
        </div>
    );
}