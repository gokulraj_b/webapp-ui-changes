import React from 'react'
import { Card } from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import Moment from 'react-moment';

import configData from "../../../../config.json";
import '../SellerProductsPage.css'

export default function SellerProduct(props) {

    const renderImage = (image) => {
        
        if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
        return `${configData.DEFAULT_PRODUCT_IMG}`
    }

    return (
        <div className="seller-product-item-card">
            <Card style={{ width: '80%' }}>
                <Card.Header>
                    <a className="seller-product-item-name" href={`/product/${props.productId}`}> {props.name} </a>
                    <div className="seller-product-category-header">{props.category} ({props.breed} Breed)</div>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        <div className="seller-product-item-card-text">
                            <div className="seller-product-img-div">
                                <Image className="seller-product-img" src={renderImage(props.productImage[0].image)} fluid />
                            </div>
                            <div className="seller-product-total-div">
                                <div>
                                    {props.text}
                                </div>
                                <div>
                                    {props.gender}
                                </div>
                                <div>
                                    Original Price: <span> </span> &#8377; {props.orignialprice} || Offer Price: <span> </span> &#8377; {props.discountprice}
                                </div>
                            </div>
                            <div className='seller-product-list-detail'>
                                <br />
                                <div>
                                    {props.issoldout ?
                                        <div className='seller-product-list-sold'> SOLDOUT </div>
                                        :
                                        <div>  Stock(s) available: <span> </span> {props.stockavailable} </div>
                                    }
                                </div>
                                <div>
                                    Listed on: <span> </span> <Moment format="DD-MMMM-yyyy">{props.createdon}</Moment>
                                </div>
                            </div>
                        </div>
                    </Card.Text>
                </Card.Body>
            </Card>
            <br />
        </div>
    )
}