import React, { useEffect, useState } from 'react'
import { Card, Container, Row, Col, Button  } from 'react-bootstrap';
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import SellerProduct from './Sections/SellerProduct';
import Loader from '../../Loader'

import './SellerProductsPage.css'
import SignInModal from '../User/SignInModal';
import configData from '../../../config.json'

export default function SellerProductsPage() {

    const [isLoading, setLoading] = useState(true);
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };

    const handleShow = () => setShow(true);
   
    const [SoldProductList, setSoldProductList] = useState({})
    const [InStockProductList, setInStockProductList] = useState({})

    useEffect(() => {
        if (Cookies.get('id')) {
            let usertype = CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)
            fetch(`/api/v1/sellerproducts/seller=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}&type=${usertype}&getsolditems=true&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    console.log(resp.length);
                    setSoldProductList(resp);
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
            
            fetch(`/api/v1/sellerproducts/seller=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}&type=${usertype}&getsolditems=f&top=0`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    console.log(resp.length);
                    setInStockProductList(resp);
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
        }
        setLoading(false);
    }, [])

    return (
        <div>
            {isLoading ? <Loader /> :
                <div>
                    <Container className="seller-product-container">
                        <Tabs position="left" defaultActiveKey={1} tabWidth={3}>
                            <Tab eventKey={1} className="sold-product-detail-title" title="Sold Products">
                                <br />
                                {SoldProductList && SoldProductList.length > 0 ?
                                    <div className="seller-product-card-list">
                                            {SoldProductList.map((product, id) => {
                                                return <div>
                                                    <SellerProduct
                                                        productImage={product.product_variant[0].product_images}
                                                        key={product.id}
                                                        text={product.description}
                                                        productId={product.id}
                                                        category={product.category}
                                                        isp={product.product_variant[0].is_pregnant ? "Pregnant" : ""}
                                                        name={product.name}
                                                        breed={product.breed}
                                                        issoldout={true}
                                                        createdon={product.created_date}
                                                        gender={product.product_variant[0].gender}
                                                        stockavailable={product.product_variant[0].stock}
                                                        milkproduce={product.product_variant[0].milk_produce_per_Day}
                                                        offsprings={product.product_variant[0].no_of_offsprings}
                                                        discountprice={product.product_variant[0].offer_price}
                                                        orignialprice={product.product_variant[0].original_price}
                                                        weight={product.product_variant[0].weight}
                                                        city={product.city}
                                                        state={product.state}
                                                    />
                                                    <br />
                                                </div>
                                            })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!SoldProductList || SoldProductList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="seller-product-empty-login-link-div">
                                                            <Button className="seller-product-empty-login-link" onClick={handleShow}>Signin here to view your listed product details </Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            Your Product list is empty
                                                        </div>
                                                        <div className="seller-product-empty-message">
                                                            You can list and sell any livestock, livestock accesseries and related products from any where in india through our website in <a className="seller-product-empty-buy-link" href="/sell">here</a>. And we also take care of your product delivery for areas we serve.
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                            <Tab eventKey={2} className="sold-product-detail-title" title="Products with available stocks">
                                <br />
                                {InStockProductList && InStockProductList.length > 0 ?
                                    <div className="seller-product-card-list">
                                        
                                        {InStockProductList.map((product, id) => {
                                            return <div>
                                                <SellerProduct
                                                    productImage={product.product_variant[0].product_images}
                                                    key={product.id}
                                                    text={product.description}
                                                    productId={product.id}
                                                    category={product.category}
                                                    isp={product.product_variant[0].is_pregnant ? "Pregnant" : ""}
                                                    name={product.name}
                                                    breed={product.breed}
                                                    issoldout={false}
                                                    createdon={product.created_date}
                                                    gender={product.product_variant[0].gender}
                                                    stockavailable={product.product_variant[0].stock}
                                                    milkproduce={product.product_variant[0].milk_produce_per_Day}
                                                    offsprings={product.product_variant[0].no_of_offsprings}
                                                    discountprice={product.product_variant[0].offer_price}
                                                    orignialprice={product.product_variant[0].original_price}
                                                    weight={product.product_variant[0].weight}
                                                    city={product.city}
                                                    state={product.state}
                                                />
                                            </div>
                                        })}
                                    </div>
                                    : null
                                }
                                <Row>
                                    <Col>
                                        {!InStockProductList || InStockProductList.length == 0 ?
                              
                                            <div style={{
                                                width: '100%', display: 'flex', flexDirection: 'column',
                                                justifyContent: 'center'
                                            }}>

                                                {Cookies.get('name') == null ?
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            You have not logged in yet!
                                                        </div>
                                                        <div className="seller-product-empty-login-link-div">
                                                            <Button className="seller-product-empty-login-link" onClick={handleShow}>Signin here to view your listed product details </Button>
                                                        </div>
                                                        <div>
                                                            <SignInModal
                                                                show={show}
                                                                setModalClose={handleClose}
                                                                onHide={handleClose}
                                                            />
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="seller-product-empty-header">
                                                            Your Product list is empty
                                                        </div>
                                                        <div className="seller-product-empty-message">
                                                            You can list and sell any livestock, livestock accesseries and related products from any where in india through our website in <a className="seller-product-empty-buy-link" href="/sell">here</a>. And we also take care of your product delivery for areas we serve.
                                                        </div>
                                                    </div>
                                                }
                                        
                                            </div>
                                            : null
                                        }
                                    </Col>
                                </Row>
                            </Tab>
                        </Tabs>
                    </Container>
                </div>
            }
        </div>
    )
}