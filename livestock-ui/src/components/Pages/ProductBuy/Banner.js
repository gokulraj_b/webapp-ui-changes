import React from 'react';

import './Banner.css';
export default function Banner(props) {
    
    return (
        <div className="boxes">
            <li className="cards__item">
                <a className="cards__item__link" href={`/product/${props.productId}`}>
                {/* <Link className="cards__item__link" to={{ pathname: '/product/', productId: props.productId }} style={{ textDecoration: 'none' }}> */}
                    <figure className="cards__item__pic--wrap" data-category={props.label}>
                        <figure className={props.isp === "" ? '' : 'cards_item_pic--wrap--pregnant'} is-pregnant={props.isp}>
                            <figure className="cards_item_pic--wrap--new" is-new={props.new}>
                                <img src={props.src} alt="Items images"
                                    className="cards__item__img" />
                            </figure>
                        </figure>
                    </figure>
                    <div className="cards__item__info">
                        <div className="cards__item__name">{props.name}</div>
                        <div className="cards__item__about">{props.breed + " | " + props.weight + " Kgs of weight"}</div>
                        <div className="cards__item__gender_milkproduce">{props.gender}</div>
                        <div className="cards__item__gender_milkproduce">{props.milkproduce != 0 ? " | Produces " + props.milkproduce + " lts of milk everyday (avg)" : null}</div>
                        <br />
                        <div className="cards__item__originalprice">&#8377;{props.orignialprice}</div>
                        <div className="cards__item__discountprice">&#8377;{props.discountprice}</div>
                        <div className="cards__item__text">
                            {props.city + ", " + props.state}
                        </div>
                    </div>
                {/* </Link> */}
                </a>
            </li>
        </div>
    )
}