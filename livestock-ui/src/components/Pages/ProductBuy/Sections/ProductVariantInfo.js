import React, { useEffect, useState } from 'react'
import { Card, Col, Container, Row, Button } from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import Moment from 'react-moment';
import { Label } from 'reactstrap'
import DatePicker from "react-datepicker";
import { FcApproval, FcCancel } from "react-icons/fc";
import {GiStethoscope, GiLoveInjection} from "react-icons/gi"
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import '../Banner.css'

export default function ProductVariantInfo(props) {

    const [Product, setProduct] = useState([])
    const [VetCheckDoneBy, setVetCheckDoneBy] = useState(null)
    const [VetCheckApprove, setVetCheckApprove] = useState(false)
    const [VetCheckReject, setVetCheckReject] = useState(false)
    const [VetCheckDoneDate, setVetCheckDoneDate] = useState(new Date())
    const [VetCheckComment, setVetCheckComment] = useState(null)
    const [VetCheckDateFieldValidationError, setVetCheckDateFieldValidationError] = useState(null)
    const [VetCheckCommentFieldValidationError, setVetCheckCommentFieldValidationError] = useState(null)
    const [VetCheckStatusFieldValidationError, setVetCheckStatusFieldValidationError] = useState(null)
    const [SaveVetCheckDetailResponseData, setSaveVetCheckDetailResponseData] = useState(null)
    const [SaveVetCheckDetailError, setSaveVetCheckDetailError] = useState(null)
    

    const handleVetCheckComment = (e) => {
        setVetCheckComment(e.target.value)
    }

    const clearErrorMessages = () => {
        setSaveVetCheckDetailError(null);
        setVetCheckDateFieldValidationError(null)
        setVetCheckCommentFieldValidationError(null)
        setVetCheckStatusFieldValidationError(null)
    }

    const saveVetCheckInput = (requestJson) => {
        fetch('/api/v1/livestock-veterinary-check', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestJson),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.json());
                }
            })
            .then((result) => setSaveVetCheckDetailResponseData(result.rows))
            .catch((err) => {
                console.log(err);
                setSaveVetCheckDetailError("Failed to save Veterinary check details.");
                setVetCheckComment(null);
                setVetCheckApprove(false);
                setVetCheckReject(false);
                setVetCheckDoneDate(new Date());
            })
    }
    
    const handleVetCheckResponseSubmit = () => {
        clearErrorMessages()

        if (!VetCheckApprove && !VetCheckReject) {
            setVetCheckStatusFieldValidationError("Approval status value is required.")
            return
        }
        if (!VetCheckDoneDate) {
            setVetCheckDateFieldValidationError("Checked-On field value is required.")
            return
        }
        if (!VetCheckComment) {
            setVetCheckCommentFieldValidationError("Comments field value is required.")
            return
        }

        const jsonData = {
            veterinary_check: props.productId,
            veterinary: props.loggedInUserId,
            checked_on: VetCheckDoneDate,
            is_vet_approved: VetCheckApprove,
            vet_comments: VetCheckComment
        };

        saveVetCheckInput(jsonData)
    }


    useEffect(() => {

        if (props.vetCheckDetail && props.vetCheckDetail.length > 0) {
            setVetCheckComment(props.vetCheckDetail[0].vet_comments)
            setVetCheckApprove(props.vetCheckDetail[0].is_vet_approved)
            setVetCheckReject(!props.vetCheckDetail[0].is_vet_approved)
            setVetCheckDoneDate(props.vetCheckDetail[0].checked_on)
            setVetCheckDoneBy(props.vetCheckDetail[0].veterinary)
        }

        setProduct(props.detail)

    }, [props.detail])

    return (
        <div>
            <Container>
                <Tabs position="left" defaultActiveKey={1} tabWidth={3}>
                    <Tab eventKey={1} className="product-detail-title" title="Product Variant detail">
                        <br />
                        <Row>
                            <Col>
                                <Table className="table-productVariant-detail" striped bordered hover size="sm">
                                    <tbody>
                                        <tr>
                                            <td>
                                                Breed
                                            </td>
                                            <td>
                                                {props.breed}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Age
                                            </td>
                                            <td>
                                                {props.age}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Gender
                                            </td>
                                            <td>
                                                {props.gender}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Color
                                            </td>
                                            <td>
                                                {props.color}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Livestock Feed type
                                            </td>
                                            <td>
                                                {props.feedtype}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Teeth Count
                                            </td>
                                            <td>
                                                {props.teethCount}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Milk Produced Per day
                                            </td>
                                            <td>
                                                {props.milkProduce ? props.milkProduce + " lts" : "-"}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Is Currently Pregnant
                                            </td>
                                            <td>
                                                {props.ispregnant ? "Yes" : "-"}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Number of Offsprings
                                            </td>
                                            <td>
                                                {props.offsprings}
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </Col>
                            <Col>
                            </Col>
                        </Row>
                            
                    </Tab>
                    <Tab eventKey={2} title="Livestock Vaccination detail">
                        <br />
                        <Row>
                            {props.vaccinationDone && props.vaccinationDone.length > 0 ?
                                props.vaccinationDone.map((vaccine, index) => (
                                    <Col key={index} sm={4} md={4} lg={3}>
                                        <Card style={{ width: '18rem' }}>
                                            <Card.Header>{vaccine.vaccine}</Card.Header>
                                            <Card.Body>
                                                <Card.Subtitle className="mb-2 text-muted">{"Completed dosages: " + vaccine.completed_dose} </Card.Subtitle>
                                                <Card.Text>
                                                    Latest vaccinated date:<span> </span>
                                                    <Moment format="DD-MMMM-yyyy">{vaccine.vaccinated_on}</Moment>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                )
                                )
                                :
                                <div className='product-detail-vaccination-yet-to-done'>
                                    <GiLoveInjection /> <span>  </span>
                                    Vacinnation information is not available for this product
                                </div>
                            }
                        </Row>
                    </Tab>
                    <Tab eventKey={3} title="Veterinary Check Details">
                        <br />
                        <Row>
                            {props.pickup && props.loggedInUserType == '5' ?
                                <Col sm={4} md={4} lg={3}>
                                    <Card style={{ width: '18rem' }}>
                                        <Card.Header>PickUp address</Card.Header>
                                        <Card.Body>
                                            <Card.Subtitle className="mb-2 text-muted">{"Type: " + props.pickup.address.type}</Card.Subtitle>
                                            <Card.Text>
                                                {props.pickup.address.address_1}
                                                <br />
                                                {props.pickup.address.address_2}
                                                <br />
                                                {props.pickup.address.city + ", " + props.pickup.address.state}
                                                <br />
                                                {props.pickup.address.country + " - " + props.pickup.address.pincode}
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                                : null
                            }
                            <Col sm={4} md={4} lg={3}></Col>
                            <Col>
                                {(props.vetCheckDetail && props.vetCheckDetail.length > 0) || SaveVetCheckDetailResponseData ?
                                    <div>
                                        Vet Check: {VetCheckApprove ?
                                            <span><FcApproval /> Done</span>
                                            :
                                            <span> <FcCancel /> Rejected</span>
                                        }
                                        <br />
                                        Vet Comments: <span> </span>
                                        {VetCheckComment}
                                        <br />
                                        Check Done By: <span> </span>
                                        {VetCheckDoneBy}
                                        <br />
                                        Check Done On: <span> </span>
                                        <Moment format="DD-MMMM-yyyy">{VetCheckDoneDate}</Moment>
                                    </div>
                                    :
                                    <div>
                                        {
                                            props.loggedInUserType == '5' ?
                                                <div>
                                                    {SaveVetCheckDetailError ?
                                                        <div className="error">
                                                            {SaveVetCheckDetailError}
                                                            <br />
                                                        </div>
                                                        : null
                                                    }
                                                    Is livestock approved: <span> </span>
                                                    <span>  </span>
                                                    <input
                                                        name="VetCheckApproved"
                                                        type="checkbox"
                                                        checked={VetCheckApprove}
                                                        onChange={(e) => { setVetCheckApprove(e.target.checked); setVetCheckReject(!e.target.checked) }} />
                                                    <span>  </span>
                                                    <Label>Yes</Label>
                                                    <span> / </span>
                                                    <span>  </span>
                                                    <input
                                                        name="VetCheckReject"
                                                        type="checkbox"
                                                        checked={VetCheckReject}
                                                        onChange={(e) => { setVetCheckReject(e.target.checked); setVetCheckApprove(!e.target.checked) }} />
                                                    <span>  </span>
                                                    <Label>No</Label>
                                                    {VetCheckStatusFieldValidationError ?
                                                        <span className="error" >
                                                            <br />
                                                            {VetCheckStatusFieldValidationError}
                                                        </span>
                                                        : null}
                                                    <br />

                                                    <Label>
                                                        Comments:
                                                    </Label>
                                                    <span> </span>
                                                    {VetCheckCommentFieldValidationError ?
                                                        <span className="error" >
                                                            <br />
                                                            {VetCheckCommentFieldValidationError}
                                                        </span>
                                                        : null}
                                                    <textarea className="form-control" onChange={handleVetCheckComment}
                                                        value={VetCheckComment} placeholder="Enter your Comments" />
                                                    <Label>Check Done on: </Label>
                                                    <span> </span>
                                                    <DatePicker className="form-control" dateFormat="dd-MM-yyyy" isClearable selected={VetCheckDoneDate} onChange={(date) => setVetCheckDoneDate(date)} />
                                                    {
                                                        VetCheckDateFieldValidationError ?
                                                            <span className="error" >
                                                                <br />
                                                                {VetCheckDateFieldValidationError}
                                                            </span>
                                                            : null
                                                    }
                                                    <br />
                                                    <br />
                                                    <Button variant="primary" onClick={handleVetCheckResponseSubmit}>Submit Response</Button>
                                                </div>
                                                : 
                                                <div className='product-detail-vet-check-yet-to-done'>
                                                    <GiStethoscope /> <span>  </span>
                                                    Veterinary confirmation yet to be completed!
                                                </div>
                                        }
                                    </div>
                                }
                            </Col>
                        </Row>
                    </Tab>
                </Tabs>
            </Container>
        </div>
    )
}