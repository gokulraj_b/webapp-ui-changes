import React, { useState } from 'react'
import Table from 'react-bootstrap/Table'
import { FaUserMd } from "react-icons/fa";
import { BsPhone } from "react-icons/bs";
import {MdEventAvailable} from "react-icons/md";
import {CgCloseO} from "react-icons/cg";
import { Label, UncontrolledTooltip } from 'reactstrap';
import { Button } from 'react-bootstrap';
import NumericInput from 'react-bootstrap-input-spinner'
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import '../Banner.css'
import SignInModal from '../../User/SignInModal';

export default function ProductInfo(props) {
    
    const [Quantity, setQuantity] = useState(1)
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };   

    const handleShow = () => setShow(true);

    const [SaveOrderResponseData, setSaveOrderResponseData] = useState(null)
    const [SaveOrderError, setSaveOrderError] = useState(null)

    const handleQuantityChange = (obj) => {
        setQuantity(obj)
    }

    const saveItemIntoBuyerCart = (requestJson) => {
        fetch(`/api/v1/buyercart/buyer=${encodeURIComponent(props.loggedInUserId)}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify(requestJson),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then((result) => {
                setSaveOrderResponseData(result.rows);
                props.showOrderSuccess()
            })
            .catch((err) => {
                setSaveOrderError("Failed to add item to cart")
                props.showOrderFail()
            })
    }

    const addToCarthandler = (evt) => {
        evt.preventDefault();
    
        const jsonData = {
            buyer: props.loggedInUserId,
            product: props.id,
            items_count: Quantity,
            total_payable: 0
        };

        saveItemIntoBuyerCart(jsonData);
    }

    return (
        <div>
            <div className="product-detail-display-inline">
            
                <div className="product-detail-title">
                    Product Info
                </div>
            
                {
                    props.vetCheck && props.vetCheck.length > 0 ?
                        <div className="product-detail-vetcheck">
                            <FaUserMd /> Vet Check Done
                        </div>
                        : null
                }

                <div className="product-detail-stock-info">
                    {props.stock && props.stock > 0 ?
                        <div className="product-detail-instock">
                            <MdEventAvailable /> In Stock
                        </div>
                        : <div className="product-detail-outofstock">
                            <CgCloseO /> Item Sold out
                        </div>
                    }
                </div>
                
            </div>
            <Table striped bordered hover size="sm">
                <tbody>
                    <tr>
                        <td>
                            Description
                        </td>
                        <td>
                            {props.description}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category
                        </td>
                        <td>
                            {props.category}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Weight
                        </td>
                        <td>
                            {props.weight ? props.weight + " Kgs" : "-"}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Price
                        </td>
                        <td>
                            <div className="product-detail-display-inline">
                                <div className="detail_discountprice">&#8377;{props.discountPrice}</div>
                                <div className="detail_originalprice">&#8377;{props.originalPrice}</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sold By
                        </td>
                        <td>
                            {props.sellerName}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Location
                        </td>
                        <td>
                            {props.location}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone
                        </td>
                        <td>
                            <div className="product-detail-display-inline">
                                <div className="product-detail-seller-phone">
                                    <BsPhone />
                                </div>
                                {props.loggedInUserId ?
                                    <div className="product-detail-seller-phone-number">
                                        {props.sellerPhone}
                                    </div>
                                    :
                                    <div>
                                        <span className='span-one' href="#" id="tooltip-sellerphone">
                                            <div className="product-detail-seller-phone-number">
                                                **********
                                            </div>
                                        </span>
                                        <UncontrolledTooltip placement="right" target="tooltip-sellerphone">
                                            Login to view Seller's phone number
                                        </UncontrolledTooltip >
                                    </div>
                                }
                               
                            </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
            <div className="product-detail-display-inline">
                <div className="product-detail-freedelivery">
                    {props.stock && props.stock > 0 && props.isdeliverable ?
                        "*Free Delivery available for this product"
                        :
                        null
                    }
                </div>
            </div>
            <div className="product-detail-min-stock">
                {props.stock && props.stock > 0 && props.stock < 5 ?
                    <Label>Hurry! Only {props.stock} left</Label> : null}
            </div>
            <div className="product-detail-display-inline">
                {props.stock && props.stock > 0 ?
                    <div className="product-detail-quantity-value">
                        <NumericInput
                            type={'real'}
                            variant={'secondary'}
                            size="sm"
                            step={1}
                            min={1}
                            max={props.stock}
                            value={Quantity}
                            onChange={handleQuantityChange}
                        />
                    </div>
                    : null}
                <div className="btn-addtocart">
                    {props.loggedInUserId ?
                        <div>
                            
                            {props.stock && props.stock > 0 && props.isdeliverable && !props.isSellerViewing ?
                                <Button className="btn btn-danger" onClick={addToCarthandler}>Add to cart</Button>
                                : null
                            }
                        </div>
                        : <Button onClick={handleShow}>Login to add item to your cart</Button>
                    }
                </div>
                <div>
                    <SignInModal
                        show={show}
                        setModalClose={handleClose}
                        onHide={handleClose}
                    />
                </div>
            </div>
            <div>
                {!props.isdeliverable && !props.isSellerViewing ?
                    <Label className='warn'>Sorry, this Product cannot be delivered, our delivery service is not available in this area now. But you can always contact the seller through phone.</Label>
                    : null
                }
            </div>
        </div>
      
    )
}