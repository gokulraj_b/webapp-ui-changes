import React, { useEffect, useState } from "react";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Cookies from 'js-cookie'

import Banner from './Banner';
import './Banner.css';
import Loader from '../../Loader'

import configData from "../../../config.json";
// import ct from '../Home/images/slide1.jpg';

import { Container } from "react-bootstrap";

export default function ProductList(props) {
    
    const [isLoading, setLoading] = useState(true);
    const [productDetail, setProductDetail] = useState([]);

    useEffect(() => {
        fetch(`/api/v1/product-detail-list/${encodeURIComponent("top=0&category=" + props.type + "&page=buy")}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setProductDetail(resp);
                setLoading(false);
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
            })
    }, []);
    
    const getproductimage = (productvariantimages) => {
        if (productvariantimages && productvariantimages.length > 0 && productvariantimages[0].image) {
            return `${configData.IMG_FOLDER_PATH}${productvariantimages[0].image}`
        } else {
            return `${configData.DEFAULT_PRODUCT_IMG}`
        }
    }
    
    return (
        <div>
            {isLoading ?
                <Loader />
                :
                <div>
                    {productDetail && productDetail.length > 0 ?
                        <Container>
                            <div className="cards__container">
                                <div className="cards__wrapper">
                                    <Row className="g-4">
                                        {productDetail.map((product, id) => (
                                            <Col>
                                                <div>
                                                    <Banner
                                                        key={id}
                                                        src={getproductimage(product.product_variant[0].product_images)}
                                                        key={product.id}
                                                        text={product.description}
                                                        productId={product.id}
                                                        label={product.category}
                                                        isp={product.product_variant[0].is_pregnant ? "Pregnant" : ""}
                                                        new="newest"
                                                        path="/Product-Detail"
                                                        name={product.name}
                                                        breed={product.breed}
                                                        gender={product.product_variant[0].gender}
                                                        milkproduce={product.product_variant[0].milk_produce_per_Day}
                                                        offsprings={product.product_variant[0].no_of_offsprings}
                                                        discountprice={product.product_variant[0].offer_price}
                                                        orignialprice={product.product_variant[0].original_price}
                                                        weight={product.product_variant[0].weight}
                                                        city={product.pickup.address.city}
                                                        state={product.pickup.address.state}
                                                    />
                                                </div>
                                            </Col>
                                        )
                                        )}
                                    </Row>
                                </div>
                            </div>
                        </Container>
                        :
                        <div style={{
                            display: 'flex', flexDirection: 'column',
                            justifyContent: 'center'
                        }} className="product-list-empty">
    
                        <div className="product-list-empty-heading">
                            Sorry! there are no products in the list that matches your search condition as of now, please try later again or try with other conditions.
                        </div>
                        </div>
                        
                    }
                </div>
            }
        </div>
    )
}