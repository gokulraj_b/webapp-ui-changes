import React from 'react'
import { useParams } from 'react-router';
import ProductList from './ProductList'

export default function Buy() {
  const { typeId } = useParams()
  return (
    <div>
      <ProductList type={typeId} />
    </div>
  );
}