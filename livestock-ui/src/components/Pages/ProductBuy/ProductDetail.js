import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal'
import ProductImage from './Sections/ProductImage';
import ProductInfo from './Sections/ProductInfo';
import ProductVariantInfo from './Sections/ProductVariantInfo';
import { IoMdCheckmarkCircleOutline, IoMdCloseCircleOutline } from 'react-icons/io';

import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import './Banner.css';
import configData from '../../../config.json'

export default function ProductDetail(props) {
    
    const { productId } = useParams()
    
    const [isdeliverable, setIsdeliverable] = useState(false);
    const [productDetail, setProductDetail] = useState([]);
    const [sellerName, setSellerName] = useState(null)
    const [sellerId, setSellerId] = useState(null)
    const [sellerPhone, setSellerPhone] = useState(null)
    const [isSellerViewing, setIsSellerViewing] = useState(false)
    const [pickupCity, setPickupCity] = useState(null)
    const [pickupState, setPickupState] = useState(null)
    //const [pickupPincode, setPickupPincode] = useState(null);
    //const [deliverablePincodes, setDeliverablePincodes] = useState([]);
    const [productVariantDetail, setProductVariantDetail] = useState([]);
    const [productImage, setProductImage] = useState([]);
    const [showOrderSuccessMessage, setshowOrderSuccessMessage] = useState(false)
    const [showOrderErrorMessage, setshowOrderErrorMessage] = useState(false)

    const showSaveOrderSuccess = () => {
        setshowOrderSuccessMessage(true)
    }

    const showSaveOrderError = () => {
        setshowOrderErrorMessage(true)
    }

    const checkifdeliverable = (pickpup_pincode) => {
        // fetching deliverable pincode details
        fetch("/api/v1/deliverable-pincodes", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                //setDeliverablePincodes(resp)
                if (resp && resp.length > 0) {
                    let deliverablePincodes = resp
                    for (let deliverypincode of deliverablePincodes) {
                        if (deliverypincode.pincode == pickpup_pincode) {
                            setIsdeliverable(deliverypincode.isdeliverable)
                            break
                        }
                    }
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }
    
    useEffect(() => {

        fetch(`/api/v1/product/${encodeURIComponent("id=" + productId)}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp[0]);
                setProductDetail(resp[0]);
                setPickupCity(resp[0].pickup.address.city)
                setPickupState(resp[0].pickup.address.state)
                setSellerName(resp[0].seller.username)
                setSellerPhone(resp[0].seller.phone)
                setSellerId(resp[0].seller.id)
                setProductVariantDetail(resp[0].product_variant[0])
                setProductImage(resp[0].product_variant[0].product_images)
                checkifdeliverable(resp[0].pickup.address.pincode)
                if (resp[0].seller.id == CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)) {
                    setIsSellerViewing(true)
                }                
            })
            .catch((err) => {
                console.log(err);
            })
    }, []);

    return (
        <div className="postPage" style={{ width: '100%', padding: '3rem 4rem' }}>

            <div className="product-detail-name">
                {productDetail.name}
            </div>
            <div>
                <div>
                    <Modal
                        size="lg"
                        centered
                        backdrop="static"
                        show={showOrderSuccessMessage}
                    >
                        <Modal.Header>
                            <Modal.Title id="Order-Item-save-response">
                                <div className="modal-success modal-message-title">
                                    Congrats! Item added
                                </div>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modal-success modal-message-content">
                                <IoMdCheckmarkCircleOutline /> The selected item has been successfully added to your cart.
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <div className="product-detail-btn">
                                <Link to="/buy" className="btn btn-primary">Continue Buy</Link>
                                <span> </span>
                                <Link to="/my-cart" className="btn btn-primary">View Mycart</Link>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </div>
                
                <div>
                    <Modal
                        size="lg"
                        centered
                        backdrop="static"
                        show={showOrderErrorMessage}
                    >
                        <Modal.Header>
                            <Modal.Title id="Order-Item-save-response-error">
                                <div className="modal-failure modal-message-title">
                                    Failed to add item
                                </div>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="modal-failure modal-message-content">
                                <IoMdCloseCircleOutline /> Sorry! There seems to be some issue while adding selected item to your cart, please <Link className="help-link" to="/support"> contact our support team </Link> for more details.
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <div className="product-detail-btn">
                                <Link to="/buy" className="btn btn-primary">Continue Buy</Link>
                            </div>
                        </Modal.Footer>
                    </Modal>
                </div>
                    
            </div>

            <br />
            <Container>
                <Row gutter={[10, 10]} >
                    <Col>
                        <div className="product-detail-image">
                            <ProductImage detail={productImage} />
                        </div>
                    </Col>
                    <Col>
                        <ProductInfo
                            id={productDetail.id}
                            loggedInUserId={Cookies.get('id') ? CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null}
                            loggedInUserName={Cookies.get('name') ? CryptoJS.AES.decrypt(Cookies.get('name'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null}
                            description={productDetail.description}
                            vetCheck={productDetail.veterinary_check}
                            sellerName={sellerName}
                            sellerPhone={sellerPhone}
                            sellerId={sellerId}
                            location={pickupCity + ', ' + pickupState}
                            category={productDetail.category}
                            weight={productVariantDetail.weight}
                            originalPrice={productVariantDetail.original_price}
                            discountPrice={productVariantDetail.offer_price}
                            stock={productVariantDetail.stock}
                            detail={productDetail}
                            isdeliverable={isdeliverable}
                            isSellerViewing={isSellerViewing}
                            showOrderSuccess={showSaveOrderSuccess}
                            showOrderFail={showSaveOrderError}
                        />
                    </Col>
                </Row>
                <br />
                <Row gutter={[16, 16]} >
                    <Col lg={12} xs={24}>
                        <ProductVariantInfo
                            loggedInUserId={Cookies.get('id') ? CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null}
                            loggedInUserName={Cookies.get('name') ? CryptoJS.AES.decrypt(Cookies.get('name'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null}
                            loggedInUserType={Cookies.get('type') ? CryptoJS.AES.decrypt(Cookies.get('type'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8) : null}
                            productId={productDetail.id}
                            breed={productDetail.breed}
                            age={productVariantDetail.age}
                            gender={productVariantDetail.gender}
                            color={productVariantDetail.color}
                            feedtype={productVariantDetail.feedtype}
                            teethCount={productVariantDetail.adult_teeth_pair_count}
                            milkProduce={productVariantDetail.milk_produce_per_Day}
                            ispregnant={productVariantDetail.is_pregnant}
                            offsprings={productVariantDetail.no_of_offsprings}
                            vaccinationDone={productDetail.vaccinated}
                            pickup={productDetail.pickup}
                            vetCheckDetail={productDetail.veterinary_check}
                            // addToCart={addToCartHandler}
                            detail={productDetail} />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}