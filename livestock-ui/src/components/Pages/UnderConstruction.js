import React from 'react'
import configData from "../../config.json";

import './UnderConstruction.css'

export default function UnderConstruction() {

    const getbackgroundimage = () => {
        return `url(/comming_soon_bg.jpg)`
    }

    return (
        <div style={{
            backgroundImage: getbackgroundimage(), backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',            
            height: '100vh'
        }} >
            <div className="center-heading">
                <h1>
                    Comming Soon!
                </h1>
            </div>
            <div className="split-line">
                <hr style={{
                    height: 8
                }} />
            </div>
            <div className="center-sentence">
                <h5>
                    This page is under construction. Will be launched soon.
                </h5>
            </div>
        </div>
    )
}