import React, { useEffect, useState } from 'react'
import { Container, Row, Col, Button, OverlayTrigger, Tooltip, Alert } from "react-bootstrap";
import { useParams } from 'react-router-dom';
import { Label } from "reactstrap";
import Select from "react-select";
import PhoneInput from 'react-phone-input-2'

import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'
import Image from 'react-bootstrap/Image'
import Card from 'react-bootstrap/Card'
import { FaUserEdit, FaUserCheck, FaUserSlash } from "react-icons/fa";
import { MdAddAPhoto } from "react-icons/md";

import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import './UserProfile.css'
import 'react-phone-input-2/lib/style.css'
import { IoMdCheckmarkCircleOutline } from 'react-icons/io';

import configData from "../../../config.json";

export default function UserProfile(props) {
   
    const { userId } = useParams()

    const [saveSuccess, setSaveSuccess] = useState(false)
    const [validationErrors, setValidationErrors] = useState({})

    const [IsEditingProfile, setIsEditingProfile] = useState(false)
    const [IsEditingImg, setIsEditingImg] = useState(false)
    const [userProfile, setUserProfile] = useState({})

    const [userTypeList, setuserTypeList] = useState([])
    const [userTitleList, setuserTitleList] = useState([])

    const [ExistingUserPhone, setExistingUserPhone] = useState(null)
    const [ExistingUserProfilePhoto, setExistingUserProfilePhoto] = useState(null)
    const [ExistingUserTypeName, setExistingUserTypeName] = useState(null)
    const [ExistingInternalUserType, setExistingInternalUserType] = useState(null)
    const [ExistingUserTitleName, setExistingUserTitleName] = useState(null)
    const [ExistingUserName, setExistingUserName] = useState(null)
    const [ExistingUserEmail, setExistingUserEmail] = useState(null)
    const [ExistingIsUserActive, setExistingIsUserActive] = useState(null)

    const [UserPhone, setUserPhone] = useState(null)
    const [UserType, setUserType] = useState(null)
    const [UserTitle, setUserTitle] = useState(null)
    const [UserProfilePhoto, setUserProfilePhoto] = useState(null)
    const [InternalUserType, setInternalUserType] = useState(null)
    const [UserName, setUserName] = useState(null)
    const [UserEmail, setUserEmail] = useState(null)
    const [IsUserActive, setIsUserActive] = useState(null)

    const [UploadedImg, setUploadedImg] = useState(null)

    const handleUserNameChange = (e) => setUserName(e.value)


    const handleSuccessProceed = (e) => {
        e.preventDefault();
        setSaveSuccess(false)
        window.location.reload()
    }

    const handleEditButtonClick = (e) => {
        //calling user type api on edit button clickchange
        fetch("/api/v1/user-type", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp)
                setuserTypeList(resp);
            })

        //calling user title api on edit button clickchange
        fetch("/api/v1/user-title", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp)
                setuserTitleList(resp);
            })

        setIsEditingProfile(true)
    }

    const handleEditImgClick = (e) => setIsEditingImg(true)

    const handleCancelButtonClick = (e) => {
        setIsEditingProfile(false)
        setIsEditingImg(false)
        setUploadedImg(null)
    }

    useEffect(() => {
        fetch(`/api/v1/userprofile/id=${encodeURIComponent(userId)}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setUserProfile(resp);
                if (resp.is_staff) {
                    setExistingInternalUserType('1')
                    setInternalUserType('1')
                } else if (resp.is_admin) {
                    setExistingInternalUserType('2')
                    setInternalUserType('2')
                } else if (resp.is_superuser) {
                    setExistingInternalUserType('3')
                    setInternalUserType('3')
                }

                setExistingUserEmail(resp.email)
                setExistingUserName(resp.username)
                setExistingUserPhone(resp.phone)
                setExistingUserProfilePhoto(resp.user_image[0]?.image)
                setExistingUserTypeName(resp.usertypename)
                setExistingUserTitleName(resp.usertitlename)
                setExistingIsUserActive(resp.is_active)

                setUserEmail(resp.email)
                setUserName(resp.username)
                setUserPhone(resp.phone)
                setUserProfilePhoto(resp.user_image[0]?.image)
                setUserType(resp.usertype)
                setUserTitle({ id: resp.title, title: resp.usertitlename })
                setIsUserActive(resp.is_active)
            })
            .catch((err) => {
                console.log(err);
            })
    }, []);

    const handleUserTitleChange = (obj) => {
        console.log(obj)
        setUserTitle(obj)
    }

    const internalUserType_radios = [
        { name: 'Staff', value: '1' },
        { name: 'Admin', value: '2' },
        { name: 'Super Admin', value: '3' }
    ];

    const renderImage = (uploadImage, image) => {
        
        if (uploadImage) {
            return URL.createObjectURL(uploadImage)
        } else if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
        return `/default_profile_picture.jpg`
    }

    const handlefileuploadChange = (e) => {
        if (e.target.files) {
            setUploadedImg(e.target.files[0])
        }
    }

    const handleProfileImageSubmit = (e) => {
        
        e.preventDefault()

        let data = new FormData()

        data.append('user', userId);
        data.append('image', UploadedImg)


        fetch(`/api/v1/userprofileimage`, {
            method: "POST",
            headers: {
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: data
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setSaveSuccess(true)
            })
            .catch((err) => {
                console.log(err);
                setValidationErrors({ saveFail: "Failure on user profile image save, Please try again later/contact our support team for more details." })
            })
    }

    const submitUserProfileEdit = (requestJson) => {

        fetch(`/api/v1/userprofile/id=${encodeURIComponent(userId)}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify(requestJson),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setSaveSuccess(true)
            })
            .catch((err) => {
                console.log(err);
                setValidationErrors({ saveFail: "Failure on user profile edit save, Please try again later/contact our support team for more details." })
            })
    }

    const handleSaveUserProfileEdit = (e) => {
        e.preventDefault()
        setValidationErrors({})
        const errors = {}

        if (!UserPhone) {
            errors.userphone = 'Phone number cannot be blank.'
            errors.haserror = true
        }
        if (!UserName) {
            errors.username = 'User name cannot be blank.'
            errors.haserror = true
        }
        if (!UserType) {
            errors.usertype = 'Type cannot be blank.'
            errors.haserror = true
        }
        if (!IsUserActive) {
            errors.isactive = 'User active status cannot be blank.'
            errors.haserror = true
        }

        setValidationErrors(errors)

        if (errors.haserror) {
            return
        }

        var JsonRequestData = {
            phone: UserPhone,
            email: UserEmail,
            username: UserName,
            is_admin: InternalUserType == '2' ? true : false,
            is_staff: ((UserType == '2' && !InternalUserType) || (InternalUserType == '1')) ? true : false,
            is_active: IsUserActive,
            is_superuser: InternalUserType == '3' ? true : false,
            title: UserTitle,
            usertype: UserType
        }
        
        submitUserProfileEdit(JsonRequestData)
    }

    return (
        <div>
            <div>
                {saveSuccess ?
                    <div className="user-profile-edit-success">
                        <Alert key={1} variant="success">
                            <div className="user-profile-edit-success-heading">
                                <IoMdCheckmarkCircleOutline /> User Profile edit has been saved successfully!
                                <hr />
                            </div>
                            <div className="add-address-successproceed-btn" >
                                <Button variant="primary" onClick={handleSuccessProceed}>Continue</Button>
                            </div>
                        </Alert>
                    </div>
                    : null
                }
            </div>
            <Container fluid className="profile-container">
                <div className="title-div">
                    Your Profile
                    <hr />
                    <div className="error">
                        {validationErrors.saveFail ? <span className="error">{validationErrors.saveFail}</span> : null}
                    </div>
                </div>
                {!IsEditingProfile && !IsEditingImg ?
                    <div className="profile-readonly">
                        <Card body >
                            <Row lg={2} className="profile-image-row">
                                <Col>

                                    {/* <div>
                                            <Image className="profile-img" src={renderImage(null, ExistingUserProfilePhoto)} />
                                    </div> */}
                                    <OverlayTrigger
                                        placement='right'
                                        overlay={
                                            <Tooltip id='upload-profile-pic-tooltip'>
                                                Upload new Profile picture
                                            </Tooltip>
                                        }>
                                        <div className="profile-img-div">
                                            <a href="#" onClick={handleEditImgClick} className="profile-img-edit-link">
                                                <div>
                                                    <Image className="profile-img" src={renderImage(null, ExistingUserProfilePhoto)} />
                                                </div>
                                                <div>
                                                    <MdAddAPhoto />
                                                </div>
                                            </a>

                                        </div>
                                    </OverlayTrigger>
                                </Col>
                            </Row>
                            <div className="profile-key-div">
                                <Row lg={3}>
                                    <Col className="profile-key-col">
                                        <Label>Name</Label>
                                    </Col>
                                    <Col>
                                        <Label>{ExistingUserTitleName + " " + ExistingUserName}</Label>
                                    </Col>
                                </Row>
                                <Row lg={3}>
                                    <Col className="profile-key-col">
                                        <Label>Phone</Label>
                                    </Col>

                                    <Col>
                                        <Label>{ExistingUserPhone}</Label>
                                    </Col>
                                </Row>

                                <Row lg={3}>
                                    <Col className="profile-key-col">
                                        <Label>Email</Label>
                                    </Col>
                                    <Col>
                                        <Label>{ExistingUserEmail}</Label>
                                    </Col>
                                </Row>
                                <Row lg={3}>
                                    <Col className="profile-key-col">
                                        <Label>Type</Label>
                                    </Col>
                                    <Col>
                                        <Label>{ExistingUserTypeName}</Label>
                                    </Col>
                                </Row>
                                <Row lg={3}>
                                    <Col className="profile-key-col">
                                        <Label>Internal user type</Label>
                                    </Col>
                                    <Col>
                                        <Label>{ExistingInternalUserType == '3' ? "Super Admin" : ExistingInternalUserType == '2' ? "Admin" : ExistingInternalUserType == '1' ? "User" : "External User"}</Label>
                                    </Col>
                                </Row>
                                <Row lg={3}>
                                    <Col className="profile-key-col">
                                        <Label>Is active</Label>
                                    </Col>
                                    <Col>
                                        <Label>{ExistingIsUserActive ?
                                            <div className="profile-active">
                                                <FaUserCheck /> Yes
                                            </div>
                                            :
                                            <div className="profile-inactive">
                                                <FaUserSlash /> No
                                            </div>}</Label>
                                    </Col>
                                </Row>
                                <Row lg={4}>
                                    <Col>
                                    </Col>
                                    <Col>
                                    </Col>
                                    <Col>
                                        <div>
                                            <Button variant="primary" onClick={handleEditButtonClick}><FaUserEdit /> Edit Profile</Button>
                                        </div>
                                    </Col>
                                </Row>
                            </div>

                        </Card>
                    </div>
                    :
                    <div>
                        {IsEditingImg ?
                            <div className="profile-img-edit">
                                <Card body >
                                    <Image className="profile-img" src={renderImage(UploadedImg, ExistingUserProfilePhoto)} fluid />
                                    <div className="profile-file-upload">
                                        <Label>Select new picture:</Label>
                                        <br />
                                        <input
                                            id="fileupload"
                                            className="fileupload-bar"
                                            type="file"
                                            multiple={false}
                                            label="File"
                                            onChange={handlefileuploadChange}
                                        />
                                    </div>
                                    <div className="profile-edit-footer">
                                        <div className="profile-edit-submit">
                                            <Button variant="primary" type="submit" onClick={handleProfileImageSubmit}>Upload
                                            </Button>
                                        </div>
                                        <div className="profile-edit-cancel">
                                            <Button variant="secondary" onClick={handleCancelButtonClick}>Cancel
                                            </Button>
                                        </div>
                                    </div>

                                </Card>
                            </div>
                            :
                            <div>
                                <div className="profile-edit">
                                    <Card body border="success" className="card-edit">
                                        <div className="profile-key-div">
                                            <Row lg={3} className="profile-edit-row">
                                                <Col>
                                                    <div className="profile-key-col-edit">
                                                        <Label>Name</Label>
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <div className="profile-edit-title-name">
                                                        <div className="profile-edit-title">
                                                            <Select
                                                                className="profile-edit-usertitle-dropdown"
                                                                placeholder="Title"
                                                                value={UserTitle}
                                                                options={userTitleList}
                                                                required={false}
                                                                getOptionLabel={(x) => x.title}
                                                                getOptionValue={(x) => x.id}
                                                                onChange={handleUserTitleChange}
                                                            />
                                                        </div>
                                                        <div className="profile-edit-name">
                                                            <input type="text" className="form-control" value={UserName} onChange={handleUserNameChange} placeholder="Enter your Name" />
                                                            {validationErrors.username ? <span className="error">*{validationErrors.username}</span> : null}
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row lg={3} className="profile-edit-row">
                                                <Col >
                                                    <div className="profile-key-col-edit">
                                                        <Label>Phone</Label>
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <PhoneInput
                                                        placeholder="Enter your Phone number"
                                                        country={'in'}
                                                        value={UserPhone}
                                                        onChange={setUserPhone} />
                                                    {validationErrors.userphone ? <span className="error">*{validationErrors.userphone}</span> : null}
                                                </Col>
                                            </Row>
                                            <Row lg={3} className="profile-edit-row">
                                                <Col>
                                                    <div className="profile-key-col-edit">
                                                        <Label>Email</Label>
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <input type="text" className="form-control" placeholder="User Mail Id" value={UserEmail} onChange={setUserEmail} />
                                                </Col>
                                            </Row>
                                            <Row lg={3} className="profile-edit-row">
                                                <Col>
                                                    <div className="profile-key-col-edit">
                                                        Type
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <ButtonGroup >
                                                        {userTypeList.map((radio, idx) => (
                                                            radio.id != 0 ?
                                                                <ToggleButton
                                                                    key={idx}
                                                                    id={`radio-${idx}`}
                                                                    type="radio"
                                                                    variant='outline-success'
                                                                    name="radio-type"
                                                                    value={radio.id}
                                                                    checked={UserType == radio.id}
                                                                    onChange={(e) => setUserType(e.currentTarget.value)}
                                                                >
                                                                    {radio.name}
                                                                </ToggleButton>
                                                                : null
                                                        ))}
                                                    </ButtonGroup  >
                                                    {validationErrors.usertype ? <span className="error">*{validationErrors.usertype}</span> : null}
                                                </Col>
                                            </Row>
                                            {!UserType || InternalUserType ?
                                                <Row lg={3} className="profile-edit-row">
                                                    <Col>
                                                        <div className="profile-key-col-edit">
                                                            Internal user type
                                                        </div>
                                                    </Col>
                                                    <Col>
                                                        <ButtonGroup >
                                                            {internalUserType_radios.map((radio, idx) => (
                                                                <ToggleButton
                                                                    key={idx}
                                                                    id={`radio-${idx}`}
                                                                    type="radio"
                                                                    variant='outline-success'
                                                                    name="radio-internal-type"
                                                                    value={radio.value}
                                                                    checked={InternalUserType == radio.value}
                                                                    onChange={(e) => setInternalUserType(e.currentTarget.value)}
                                                                >
                                                                    {radio.name}
                                                                </ToggleButton>
                                                            ))}
                                                        </ButtonGroup  >
                                                    </Col>
                                                </Row>
                                                : null
                                            }
                                            <Row lg={3} className="profile-edit-row">
                                                <Col>
                                                    <div className="profile-key-col-edit">
                                                        Active
                                                    </div>
                                                </Col>
                                                <Col>
                                                    <input type="checkbox" defaultChecked={IsUserActive} onChange={(e) => { setIsUserActive(e) }} />
                                                    {validationErrors.isactive ? <span className="error">*{validationErrors.isactive}</span> : null}
                                                </Col>
                                            </Row>
                                        </div>
                                        <div className="profile-edit-footer">
                                            <div className="profile-edit-submit">
                                                <Button variant="primary" type="submit" onClick={handleSaveUserProfileEdit} >Save
                                                </Button>
                                            </div>
                                            <div className="profile-edit-cancel">
                                                <Button variant="secondary" onClick={handleCancelButtonClick}>Cancel
                                                </Button>
                                            </div>
                                        </div>
                                    </Card>
                                </div>
                            </div>
                        }
                    </div>
                }
            </Container>
        </div>
    )
}