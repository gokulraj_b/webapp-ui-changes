import React, { useEffect, useState } from 'react'
import Cookies from 'js-cookie'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Select from "react-select";
import PhoneInput from 'react-phone-input-2'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'
import CryptoJS from 'crypto-js';

import './SignIn.css'
import configData from '../../../config.json'

export default function SignInModal(props) {

    const [showSignup, setshowSignup] = useState(false);

    const rememberMeChecked = Cookies.get('rememberMe') ? true : false;
    const [rememberMe, setRememberMe] = useState(rememberMeChecked)

    const [SaveUserSignInResponseData, setSaveUserSignInResponseData] = useState(null)
    const [SaveUserSignInErrorResponse, setSaveUserSignInErrorResponse] = useState(null)

    const handleRememberMe = () => {
        setRememberMe(!rememberMe)
        if (!rememberMe) {
            Cookies.remove('rememberMe')
        }
    };

    const [validationErrors, setValidationErrors] = useState({})

    const userPhoneInitial = Cookies.get('rememberMe') ?
        CryptoJS.AES.decrypt(Cookies.get('rememberMe'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8)
        : '';
    
    const [UserPhone, setUserPhone] = useState(userPhoneInitial)
    const [Password, setPassword] = useState(null)
    const [UserType, setUserType] = useState(null)
    const [UserName, setUserName] = useState(null)
    const [UserTitle, setUserTitle] = useState(4)

    const [UserTypeList, setUserTypeList] = useState([])
    const [UserTitleList, setUserTitleList] = useState([])
    
    const handleShowSignUp = () => {
        setshowSignup(true);
        setValidationErrors({})
    }

    const handleCloseSigUp = () => {
        setshowSignup(false);
        setValidationErrors({})
    }

    const handleUserNameChange = (e) => {
        setUserName(e.target.value)
        validationErrors.username = ""
    }
    
    const handlePhoneNumberChange = (value, data, event, formattedValue) => {
        setUserPhone(value)
        validationErrors.phone = ""
    }
    
    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
        validationErrors.password = ""
    }

    // handle change event of the Usertitle dropdown
    const handleUserTitleChange = (obj) => {
        setUserTitle(obj);
    };

    const clearFields = () => {
        
        setUserName(null)
        setUserPhone(null)
        setUserTitle(null)
        setUserType(null)
        setPassword(null)
    }

    useEffect(() => {

        if (showSignup) {
            //calling user type api on edit button clickchange
            fetch("/api/v1/user-type", {
                method: "GET",
                headers: {
                    "content-type": "application/json",
                },
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error(response);
                    }
                })
                .then(function (resp) {
                    console.log(resp)
                    setUserTypeList(resp);
                })
        
            //calling user title api on edit button clickchange
            fetch("/api/v1/user-title", {
                method: "GET",
                headers: {
                    "content-type": "application/json",
                },
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error(response);
                    }
                })
                .then(function (resp) {
                    console.log(resp)
                    setUserTitleList(resp);
                })
        }

    }, [showSignup])

    const authenticateUser = (requestJson) => {
        fetch('/api/v1/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify(requestJson),
        })
            .then(function (response) {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then((result) => {
                console.log(JSON.parse(result))
                const resultJson = JSON.parse(result)
                Cookies.set('user', CryptoJS.AES.encrypt(result, `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
                Cookies.set('id', CryptoJS.AES.encrypt(resultJson.id.toString(), `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
                Cookies.set('name', CryptoJS.AES.encrypt(resultJson.name.toString(), `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
                Cookies.set('phone', CryptoJS.AES.encrypt(resultJson.phone.toString(), `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
                if (rememberMe) {
                    Cookies.set('rememberMe', CryptoJS.AES.encrypt(resultJson.phone.toString(), `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
                }
                Cookies.set('type', CryptoJS.AES.encrypt(resultJson.usertype.toString(), `${configData.CRYPTO_SECRET_KEY}`).toString(), { path: '/' });
                setSaveUserSignInResponseData(result)
                props.setModalClose()
            })
            .catch((err) => {
                console.log(err);
                setValidationErrors({ loginFail: "Authentication failed." });
            })
    }

    const registerUser = (requestJson) => {
        fetch('/api/v1/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestJson),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then((result) => {
                setSaveUserSignInResponseData(result.rows)

                const jsonData = {
                    username: '+' + UserPhone,
                    password: Password
                }
        
                authenticateUser(jsonData)
            })
            .catch((err) => {
                console.log(err);
                setValidationErrors({ userRegisterFail: err });
            })
    }
    
    const loginSubmit = (evt) => {
        evt.preventDefault();
        setValidationErrors({})
        const errors = {}

        if (!UserPhone) {
            errors.phone = 'Phone number cannot be blank'
            errors.hasvalue = true
        }
        if (!Password) {
            errors.password = 'Password cannot be blank'
            errors.hasvalue = true
        }
        
        setValidationErrors(errors)
        
        if (errors.hasvalue) {
            return
        }

        const jsonData = {
            username: '+' + UserPhone,
            password: Password
        }

        authenticateUser(jsonData)
    }

    const RegisterSubmit = (evt) => {
        evt.preventDefault();
        setValidationErrors({})
        const errors = {}

        if (!UserPhone) {
            errors.phone = 'Phone number cannot be blank'
            errors.hasvalue = true
        }
        if (!Password) {
            errors.password = 'Password cannot be blank'
            errors.hasvalue = true
        }
        if (!UserName) {
            errors.username = 'Name cannot be blank'
            errors.hasvalue = true
        }
        if (UserType == null) {
            errors.usertype = 'Type is required'
            errors.hasvalue = true
        }

        setValidationErrors(errors)
        
        if (errors.hasvalue) {
            return
        }

        let userTitleSelected = "4"

        if (!UserTitle) {
            userTitleSelected = UserTitle.id
        }

        const jsonData = {
            password: Password,
            phone: "+" + UserPhone,
            username: UserName,
            is_admin: false,
            is_staff: false,
            is_active: true,
            is_superuser: false,
            title: userTitleSelected,
            usertype: UserType
        }

        registerUser(jsonData);
    }
    
    return (
        <div>
            {!showSignup ?
                <Modal
                    {...props}
                    backdrop="static"
                    keyboard={true}
                >
                    <form onSubmit={loginSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>User Login</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="form-group">
                                <label>Phone number</label>
                                {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                                <PhoneInput
                                    placeholder="Enter Phone number"
                                    country={'in'}
                                    value={UserPhone}
                                    required
                                    onChange={handlePhoneNumberChange} />
                                {validationErrors.phone ? <span className="error">*{validationErrors.phone}</span> : null}
                            </div>

                            <div className="form-group">
                                <label>Password</label>
                                <div className="sign-in-password-field">
                                    <input type="password" className="form-control" placeholder="Enter password" value={Password} onChange={handlePasswordChange} />
                                    {validationErrors.password ? <span className="error">*{validationErrors.password}</span> : null}
                                </div>
                            </div>
                        </Modal.Body>
                        {validationErrors.loginFail ? <span className="error-backend">*{validationErrors.loginFail}</span> : null}
                        <Modal.Footer>
                            <Button type="submit" className="btn btn-primary btn-lg btn-block">Login</Button>
                            <p className="register-text">
                                New user? <a href="#" onClick={handleShowSignUp}>Register here</a>
                            </p>
                        
                            <div className="form-group">
                                {/* <div className="custom-control custom-checkbox">
                                        <input type="checkbox" checked={rememberMe} onChange={handleRememberMe} className="custom-control-input" id="rememberMe" />
                                        <label className="custom-control-label" htmlFor="rememberMe">Remember me</label>
                                    </div> */}
                            </div>
                        </Modal.Footer>
                    </form>
                </Modal>
                :
                <Modal
                    {...props}
                    backdrop="static"
                    keyboard={true}
                >
                    <Modal.Header closeButton >
                        <Modal.Title>New User Registration</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form class="form-horizontal" onSubmit={RegisterSubmit}>
                            <div className="form-group">
                                <label>I am </label>
                                <div className="sign-in-usertype">
                                    <ButtonGroup >
                                        {UserTypeList.map((radio, idx) => (
                                            radio.id != 0 && radio.id != 9 ?
                                                <ToggleButton
                                                    key={idx}
                                                    id={`radio-${idx}`}
                                                    type="radio"
                                                    variant='outline-success'
                                                    name="radio"
                                                    value={radio.id}
                                                    checked={UserType == radio.id}
                                                    onChange={(e) => setUserType(e.currentTarget.value)}
                                                >
                                                    {" " + radio.name}
                                                </ToggleButton>
                                                : null
                                        ))}
                                    </ButtonGroup  >
                                </div>
                                {validationErrors.usertype ? <span className="error">*{validationErrors.usertype}</span> : null}
                            </div>
                            <div className="form-group">
                                <label>Phone number</label>
                                {/* <input type="Phone" className="form-control" placeholder="Enter Phone number" /> */}
                                <PhoneInput
                                    placeholder="Enter your Phone number"
                                    country={'in'}
                                    value={UserPhone}
                                    onChange={handlePhoneNumberChange}
                                />
                                {validationErrors.phone ? <span className="error">*{validationErrors.phone}</span> : null}
                            </div>
                            <div className="form-group">
                                <label>Name</label>
                                <div className="user-reg-name">
                                    <div className="user-reg-title-field">
                                        <Select
                                            className="user-title-dropdown"
                                            placeholder="Title"
                                            value={UserTitle}
                                            options={UserTitleList}
                                            onChange={handleUserTitleChange}
                                            getOptionLabel={(x) => x.title}
                                            getOptionValue={(x) => x.id}
                                        />
                                    </div>
                                    
                                    <div className="user-reg-name-field">
                                        <input type="text" className="form-control" value={UserName} onChange={handleUserNameChange} placeholder="Enter your Name" />
                                        {validationErrors.username ? <span className="error">*{validationErrors.username}</span> : null}
                                    </div>
                                </div>
                                    
                            </div>
                            <div>
                                <label>Password</label>
                                <div className="sign-in-password-field">
                                    <input type="password" className="form-control" placeholder="Enter password" value={Password}
                                        onChange={handlePasswordChange} />
                                    {validationErrors.password ? <span className="error">*{validationErrors.password}</span> : null}
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        {validationErrors.userRegisterFail ?
                            <span className="error">
                                *{validationErrors.userRegisterFail}
                            </span>
                            : null}
                        <Button type="submit" className="btn btn-primary btn-lg btn-block" onClick={RegisterSubmit} >Register</Button>
                        <div className="signin-text">
                            Already registered <a href="#" onClick={handleCloseSigUp}>sign in?</a>
                        </div>
                    </Modal.Footer>
                </Modal>
            }
        </div>
 
    );
}