import "./Sitedetail.css";
import CardDeck from "react-bootstrap/CardDeck";
import Card from "react-bootstrap/Card";
import supportcall from "./images/support_call.jpg";
import supportmail from "./images/emailsupport.jpg";
import whatsappsupport from "./images/whatsappsupport.jpg";

export default function Support() {
  return (
    <div className="Site-detail-support">
      <CardDeck>
        <Card>
          <Card.Img variant="top" src={supportcall} />
          <Card.Body>
            <Card.Title>Call us</Card.Title>
            <Card.Text>You can Call us at: (+91) 93434-43438</Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">
              available live on 9 AM to 7 PM from Monday to Friday
            </small>
          </Card.Footer>
        </Card>
        <Card>
          <Card.Img variant="top" src={supportmail} />
          <Card.Body>
            <Card.Title>Mail us</Card.Title>
            <Card.Text>You can Mail us at: support@farmanimalia.com</Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">
              will getback within 48 hrs on all working days
            </small>
          </Card.Footer>
        </Card>
        <Card>
          <Card.Img variant="top" src={whatsappsupport} />
          <Card.Body>
            <Card.Title>Whatsapp us</Card.Title>
            <Card.Text>You can Whatsapp us at: (+91) 944354-34532</Card.Text>
          </Card.Body>
          <Card.Footer>
            <small className="text-muted">
              will get back within 48 hrs on all working days
            </small>
          </Card.Footer>
        </Card>
      </CardDeck>
    </div>
  );
}
