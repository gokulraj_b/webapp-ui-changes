import './Sitedetail.css';

export default function AboutUs() {
    return (
        <>
            <div className="About-us">
                <p className="para-title">Our Product:</p>
                <p className="starting-para">                
                    FarmAnimalia.com is an online livestock market platform that helps farmers and buyers to trade livestock and livestock supplies in a more efficient, cost effective and secure way. Our goal is to create a safe and secure system which buyers and sellers from anywhere in India can use to  connect and increase their products' market exposure.</p>

                <p className="para-title">Problem that we solve:</p>
                <p className="starting-para">
                    As of now, if a farmer or seller wants to sell his high-quality and healthy livestock, he must contact his local agent or get a transport service by himself and he has take his livestock to a nearby physical weekly marketplace to sell  his livestock. Also, if he could not manage to sell his livestock on that day the seller has to bring back his livestock to his farm/husbandry on his own cost and so he would be foreced to sell his livestock for what ever the price he gets on the given date and because of this pressure the seller would not get the right price for his product. Also sometimes in the markets buyers might get cheated with unhealthy livestocks.</p>
                
                <p className="para-title"> Our Goal:</p>
                <p className="starting-para">
                    To reduce this painful selling and buying experience and to make the buyers and sellers work easier, FarmAnimalia helps the farmers and sellers to list their products online and find their buyer from anywhere in India and sell their high-quality livestock and livestock related products around India through its platform. Also, to make the trading easy delivering the livestock will be taken care by FarmAnimalia which will make the buyer experience also smooth and easy. We also provide vet check and seller verification facilities which will help the buyers to find the right and trusted product that they need.</p>

                <p>
                    Any buyer can find good quality livestocks, livestock accessories, feeds and medicines through FarmAnimalia.com from anywhere in India, buy them and get it delivered to their doorstep through FarmAnimalia.com.
                    We pledge to help our farmers to offer their animals to a wider circle of potential buyers and to do that in best moment to achieve maximum profit and provide buyers faster cheaper and easier way to find appropriate animals as per their needs. We help the farmers to sell their livestock from their own place without having to transport the animals around markets in search of buyers.
                </p>
                
                <p className="para-title">We stand with our Farmers:</p>
                <p className="starting-para">
                    FarmAnimalia aims to provide the biggest online market for indian farmers and livestock traders. Using farmAnimalia anyone should be able buy or sell their livestocks, livestock feeds and accessories for livestocks. Also we provide services like live Vet-on-call, Vet-at-your-doorstep, transporting agriculture products and many more services at very low cost which the farmers can utilize to achieve maximum profit.</p>
            </div>
        </>
    );
}