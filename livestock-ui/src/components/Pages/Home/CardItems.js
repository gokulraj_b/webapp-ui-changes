import React from 'react';
import {Link} from 'react-router-dom';
import './CardItem.css';
import cattle from './images/cattle.png';
import goat from './images/goat.png';
import sheep from './images/sheep.png';
import hen from './images/hen.png';
import { Container, Row, Col } from 'react-bootstrap';

export default function CardItems() {
    
    return (
        <Container>
            <div className="boxes">
                <Row>
                    <div className="carditem-box">
                        <Col>
                            <Link to="/buy/1" >
                                <div className="box" >
                                    <div className="icon"><img src={cattle} alt="cow" width="25%" height="35px"></img></div>
                                    <div className="content"><h3>Cattle</h3></div>
                                </div>
                            </Link>
                        </Col>
                        <Col>
                            <Link to="/buy/2">
                                <div className="box">
                                    <div className="icon">
                                        <img src={goat} alt="goat" width="40px" height="35px"></img>
                                    </div>
                                    <div className="content-4letter"><h3>Goat</h3></div>
                                </div>
                            </Link>
                        </Col>
                        <Col>
                            <Link to="/buy/3">
                                <div className="box">
                                    <div className="icon">
                                        <img src={sheep} alt="sheep" width="40px" height="35px"></img>
                                    </div>
                                    <div className="content"><h3>sheep</h3></div>
                                </div>
                            </Link>
                        </Col>
                        <Col>
                            <Link to="/buy/4">
                                <div className="box">
                                    <div className="icon">
                                        <img src={hen} alt="hen" width="40px" height="35px"></img>
                                    </div>
                                    <div className="content-3letter"><h3>Hen</h3></div>
                                </div>
                            </Link>
                        </Col>
                    </div> 
                </Row>
            </div>
        </Container>
    );
}