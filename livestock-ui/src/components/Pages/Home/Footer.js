import React from "react";
import "./Footer.css";
import { Link } from "react-router-dom";

export default function Footer() {
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <footer className="footer-distributed">
      <div className="footer-left">
        <Link to="/" onClick={scrollToTop}>
               <p className="footer-right" style={{textDecoration: "none"}}>FarmAnimalia.com</p>
        </Link>
        <h3>Contact Us</h3>

          <p class="footer-links">
            <a href="https://facebook.com">Facebook</a>
            |
            <a href="https://gmail.com">Gmail</a>
            |
            <a href="https://twitter.com">Twitter</a>
            |
            <a href="https://linkedin.com">LinkedIn</a>
          </p>

          <p class="footer-company-name">© 2021 farmanimalia.com. All rights reserved. | Best viewed on
            desktops and laptops</p>
			
      </div>
        <div class="footer-center">
          <p>
            <h3>Site Links</h3>
            <a>
            <Link to="/aboutus" onClick={scrollToTop}>About us </Link>
            </a><p>|</p>
            <a>
            <Link to="/how-it-works" onClick={scrollToTop}>How-it-works</Link>
            </a><p>|</p>
            <a>
              <Link to="/careers" onClick={scrollToTop}>Careers</Link>
            </a><p>|</p>
            <a>
              <Link to="/support" onClick={scrollToTop}>Support</Link>
            </a>
          </p>
          
        </div>        
    </footer>
  );
}