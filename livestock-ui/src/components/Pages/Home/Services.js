import './Service.css';
import { UncontrolledTooltip } from 'reactstrap';

export default function services() {
    
    return (
        <>
            <div className="homepage-section">
                <p className="title">OUR SERVICES </p>
                <div className="services">
                    
                    <span className="span-five" href="#" id="tooltip-buylivestock">
                        <a href="/buy/0" className="services-link">
                            <h5 className="service-circle">
                                <br />
                                Buy Livestocks
                            </h5>
                        </a>
                    </span>
                    <UncontrolledTooltip placement="bottom" target="tooltip-buylivestock">
                        Buy your livestocks at reasonable price. Search livestocks as per your need from all over india in here and get it delivered to you.
                    </UncontrolledTooltip >
                    
                    <span className="span-ten" href="#" id="tooltip-selllivestock">
                        <a href="/sell" className="services-link">
                            <h5 className="service-circle">
                                <br />
                                Sell Livestocks
                            </h5>
                        </a>
                    </span>
                    <UncontrolledTooltip placement="bottom" target="tooltip-selllivestock">
                        Sell your livestocks. Find buyer for right price here.
                    </UncontrolledTooltip>
 
                    <span className="span-ten" href="#" id="tooltip-doctorconsult">
                        <a href="/services" className="services-link">
                            <h5 className="service-circle">
                                <br />
                                Consult Vets
                            </h5>
                        </a>
                    </span>
                    <UncontrolledTooltip placement="bottom" target="tooltip-doctorconsult">
                        Consult your vetnerian online. Buy medicines through e-pharma and get it delivered to your home</UncontrolledTooltip>
 
                    <span className="span-ten" href="#" id="tooltip-livestockaccessory">
                        <a href="/services" className="services-link">
                            <h5 className="service-circle">
                                <br />
                                Accessories
                            </h5>
                        </a>
                    </span>
                    <UncontrolledTooltip placement="bottom" target="tooltip-livestockaccessory">
                        Buy livestocks accessories here and get it delivery at your doorstep.</UncontrolledTooltip>
                </div>
               
            </div>
        </>
    )
}