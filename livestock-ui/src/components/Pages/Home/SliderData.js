import slide1 from './images/slide1.jpg';
import slide2 from './images/slide2.jpg';
import slide3 from './images/slide3.jpg';
// export const SliderData =[
//     {
//         image: slide1
//     },
//     {
//         image: slide2
//     },
//     {
//         image: slide3
//     }
export const SliderData = [
    { url: slide1 },
    { url: slide2 },
    { url: slide3 }
];