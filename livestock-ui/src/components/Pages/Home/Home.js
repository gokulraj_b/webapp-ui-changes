import React from 'react'
import Categories from './Categories';
import Bann from './Bann';
import Slidercomp from './Slider';
import Services from './Services';
import Testimonials from './Testimonials'

export default function Home() {

    return (
        <div>
            <Slidercomp />
            <Services />
            <Categories />
            <Bann />
            {/* <Testimonials /> */}
        </div>
    );
}