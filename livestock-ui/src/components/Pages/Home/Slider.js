import { SliderData } from "./SliderData";
import SimpleImageSlider from "react-simple-image-slider";

export default function Slidercomp() {
  
  return (
    <div>
      <SimpleImageSlider
        width="100%"
        height={500}
        images={SliderData}
        showBullets="true"
        showNavs="true"
        useGPURender="true"
        navStyle="1"
        navSize="50"
        navMargin="30"
        duration="0.5"
        bgColor="#ffffff"
      />
    </div>
  );
}