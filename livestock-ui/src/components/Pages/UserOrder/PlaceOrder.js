import React, { useEffect, useState } from 'react'
import { Alert, Button, Card, Col, Container, Row  } from 'react-bootstrap';
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';
import OrderedItem from './Sections/OrderedItem';
import Loader from '../../Loader'
import {IoMdCheckmarkCircleOutline, IoMdCloseCircleOutline} from 'react-icons/io';

import './OrderPage.css'
import { Link, useParams } from 'react-router-dom';
import configData from '../../../config.json'

export default function PlaceOrder() {

    const { addressId } = useParams()
    const [isLoading, setLoading] = useState(true);
    const [orderPlaceSuccess, setOrderPlaceSuccess] = useState(false)
    const [orderPlaceFail, setOrderPlaceFail] = useState(false)
    
    const [orderDetail, setOrderDetail] = useState([])

    const [orderPriceTotal, setOrderPriceTotal] = useState(0)
    const [orderDeliveryTotal, setOrderDeliveryTotal] = useState(0)
    const [orderGrandTotal, setOrderGrandTotal] = useState(0)

    useEffect(() => {

        if (Cookies.get('id') != null) {
            
            fetch(`/api/v1/buyercart/buyer=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((resp) => {
                    if (resp.ok) {
                        return resp.json();
                    } else {
                        throw new Error(resp.statusText);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    setOrderDetail(resp);
                    calculateGrandTotal(resp)
                    setLoading(false);
                })
                .catch((err) => {
                    console.log(err);
                    setLoading(false);
                })
        }
        setLoading(false);
    }, [])

    const calculateTotalAmount = (order) => {
        let total = 0;
        if (order && order.length > 0) {
            order.map(item => {
                total += parseInt(item.product.product_variant[0].offer_price, 10) * item.items_count
            });
        }
        setOrderPriceTotal(total)
        return total
    }

    const calculateDelivaryAmount = (order) => {
        setOrderDeliveryTotal(0)
        return 0
    }

    const calculateGrandTotal = (order) => {
        let price = orderPriceTotal
        let delivery = orderDeliveryTotal
        if (price == 0) {
            price = calculateTotalAmount(order)
        }
        if (delivery == 0) {
            delivery = calculateDelivaryAmount(order)
        }

        setOrderGrandTotal(price + delivery)
    }

    const confirmOrderPlacement = () => {

        fetch(`/api/v1/confirm-order/buyer=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}&address=${encodeURIComponent(addressId)}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((resp) => {
                if (resp.ok) {
                    return resp.json();
                } else {
                    throw new Error(resp.statusText);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setLoading(false);
                setOrderPlaceSuccess(true)
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
                setOrderPlaceFail(true)
            })
    }

    const handleConfirmClick = (e) => {
        console.log(e)
        confirmOrderPlacement()
    }

    return (
        <div>
            {isLoading ? <Loader /> :
                <div>
                    {orderPlaceSuccess || orderPlaceFail ?
                        <div className="place-order-response-div">
                            {orderPlaceSuccess ?
                                <div>
                                    <Alert key={1} variant="success">
                                        <div className="order-heading">
                                            Your order has been placed successfully
                                            <hr />
                                        </div>
                                        <div className="order-message-content">
                                            <IoMdCheckmarkCircleOutline />  Congrats, your order confirmation has been completed, you can track your orders <Link className="help-link" to="/orders">here</Link>.
                                        </div>
                                    </Alert>
                                </div>
                                :
                                orderPlaceFail ?
                                    <div>
                                        <Alert key={1} variant="danger">
                                            <div className="order-heading">
                                                Failed to place your order
                                                <hr />
                                            </div>
                                            <div className="order-message-content">
                                                <IoMdCloseCircleOutline /> Sorry! Failure during your order confirmation, please <Link className="help-link" to="/support">contact our team</Link> for more details.
                                            </div>
                                        </Alert>
                                    </div>
                                    :
                                    null
                            }
                        </div>
                        :
                        <div className="place-order-containers">
                            <div className="order-container">
                                <Container>
                                    <div className="review-order-heading">
                                        Review your order
                                    </div>
                                    <div className="review-order-heading-edit-option">
                                        <a href="/my-cart" className="review-order-heading-edit-link"> click here</a>  to edit your cart items
                                    </div>
                                    <hr />
                                    <div className="order-card-list">
                                        <div>
                                            <Card style={{ width: '95%' }}>
                                                {orderDetail.map((orderItem, id) => {
                                                    return <div>
                                                        <OrderedItem
                                                            id={orderItem.id}
                                                            buyer={orderItem.buyer}
                                                            product={orderItem.product}
                                                            productImage={orderItem.product.product_variant[0].product_images && orderItem.product.product_variant[0].product_images.length > 0 ? orderItem.product.product_variant[0].product_images[0].image : null}
                                                            status={orderItem.status}
                                                            count={orderItem.items_count}
                                                            delivery={orderItem.delivery}
                                                            totalpayable={orderItem.total_payable}
                                                            count={orderItem.items_count}
                                                            deliveryCharge={orderItem.delivery_charge}
                                                            tax={orderItem.tax_amount}
                                                            orderedOn={orderItem.created_date}
                                                            orderPlaceSuccess={setOrderPlaceSuccess}
                                                            orderPlaceFail={setOrderPlaceFail}
                                                        />
                                                    </div>
                                                })}
                                            </Card>
                                        </div>
                                    </div>
                                </Container>
                            </div>
                                    
                            <div className="place-order-container">
                                <Container >
                                    <div>
                                        <Card border="success" style={{ width: '140%' }}>
                                            <Card.Body>
                                                <Card.Header>
                                                    <Card.Title>
                                                        <div className="place-order-title">Order summary
                                                        </div>
                                                    </Card.Title>
                                                </Card.Header>
                                                <Card.Text>
                                                    <div>
                                                        <Container className="place-order-summary-container">
                                                            <Row className="place-order-summary-row">
                                                                <Col>Price</Col>
                                                                <Col className="place-order-col-price">&#8377; {orderPriceTotal}</Col>
                                                            </Row>
                                                            <Row className="place-order-summary-row">
                                                                <Col>Delivery</Col>
                                                                <Col className="place-order-col-price">&#8377; {orderDeliveryTotal}</Col>
                                                            </Row>
                                                            <hr />
                                                            <Row className="place-order-summary-row-total">
                                                                <Col>Total payable</Col>
                                                                <Col className="place-order-col-price">&#8377; {orderGrandTotal}</Col>
                                                            </Row>
                                                        </Container>
                                                    </div>
                                                </Card.Text>
                                                <Card.Footer>
                                                    <div className="place-order-btn">
                                                        <Button variant="primary" onClick={handleConfirmClick}>Place your Order</Button>
                                                    </div>
                                                </Card.Footer>
                                                            
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Container>
                            </div>
                        </div>
                    }
                </div>
            }
        </div>
    )
}