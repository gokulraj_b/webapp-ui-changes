import React from 'react'
import { Card } from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import { GrDeliver } from "react-icons/gr";
import Moment from 'react-moment';

import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import configData from "../../../../config.json";
import '../OrderPage.css'

export default function OrderedItem(props) {

    const renderImage = (image) => {
        
        if (image) {
            return `${configData.IMG_FOLDER_PATH}${image}`
        }
        return `${configData.DEFAULT_PRODUCT_IMG}`
    }

    const confirmOrder = () => {

        fetch(`/api/v1/confirm-order/buyer=${encodeURIComponent("60e289dd-973a-4dd5-895b-6952a564f7bb")}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                props.orderPlaceSuccess(true)
            })
            .catch((err) => {
                console.log(err);
                props.orderPlaceFail(true)
            })
    }

    const handleConfirmClick = (e) => {
        confirmOrder()
    }

    return (
        <div className="order-item-card">
            <Card.Header>
                <a className="order-item-name" href={`/product/${props.product.id}`}> {props.product.name} </a>
                
                {props.status == 'Delivered' ?
                    <div className="order-delivered-header"> Delivered  <GrDeliver /></div>
                    :
                    <div className="order-inprogress-header">{props.status}</div>
                }
            </Card.Header>
            <Card.Body>
                <Card.Text>
                    <div className="order-item-card-text">
                        <div className="order-img-div">
                            <Image className="order-img" src={renderImage(props.productImage)} fluid />
                        </div>
                        <div className="order-total-div">
                            {props.product.name}
                            <div>
                                <span> </span> {props.product.description}
                            </div>
                            <div>
                                Order count: <span> </span> {props.count}
                            </div>
                            <div>
                                Total amount: <span> </span> &#8377; {props.totalpayable == 0 ? (parseInt(props.product.product_variant[0].offer_price, 10) * props.count) : props.totalpayable}
                            </div>                            
                            {/* </div>
                        <div className="order-delivery-detail-div"> */}
                            {props.status == 'Delivered' ?
                                <div className="order-delivered">
                                    Delivered On:  <span> </span>
                                    <Moment format="DD MMMM yyyy">{props.delivery.delivered_on}</Moment>
                                    
                                </div>
                                :
                                null
                            }
                        </div>
                        {props.delivery != null ?
                            <div>
                                <div className="order-delivery-address">
                                    <div className="order-delivery-address-heading">Shipping address</div>
                                    <div>{props.delivery.delivery_address.buyer}</div>
                                    <div>{props.delivery.delivery_address.address.address_1 + ', ' + props.delivery.delivery_address.address.address_2}</div>
                                    <div>{props.delivery.delivery_address.address.city + ', ' + props.delivery.delivery_address.address.state + ' - ' + props.delivery.delivery_address.address.pincode}</div>
                                    <div>Address type:
                                        <div className="order-delivery-address-type">{props.delivery.delivery_address.address.type}</div>
                                    </div>
                                </div>
                            </div>
                            : null
                        }
                    </div>
                </Card.Text>
            </Card.Body>
        </div>
    )
}