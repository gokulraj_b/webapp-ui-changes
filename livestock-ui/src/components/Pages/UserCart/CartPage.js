import React, { useEffect, useState } from 'react'
import { Button, Row, Col, Container } from 'react-bootstrap';
import { Link, useHistory } from "react-router-dom";
import NumericInput from 'react-bootstrap-input-spinner'
import Cookies from 'js-cookie'
import CryptoJS from 'crypto-js';

import Loader from '../../Loader'
import './UserCart.css'
import SignInModal from '../User/SignInModal';
import configData from "../../../config.json";

export default function CartPage() {
    
    const history = useHistory();
    
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false)
        window.location.reload()
    };

    const handleShow = () => setShow(true);

    const [isLoading, setLoading] = useState(true);
    
    const [CartItems, setCartItems] = useState([])
    const [Total, setTotal] = useState(0)
    const [ShowTotal, setShowTotal] = useState(false)
    const [ShowSuccess, setShowSuccess] = useState(false)

    const [quantity, setquantity] = useState([])
    const [initialQuantity, setInitialQuantity] = useState([])

    const [OrderError, setOrderError] = useState(null)
    
    useEffect(() => {
        
        if (Cookies.get('id') != null) {

            fetch(`/api/v1/buyercart/buyer=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}`, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                },
            })
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error(response);
                    }
                })
                .then(function (resp) {
                    console.log(resp);
                    setCartItems(resp)
                    calculateTotal(resp)
                })
                .catch((err) => {
                    console.log(err);
                })
        }
        setLoading(false);
    }, [])

    const calculateTotal = (cartDetail) => {
        let total = 0;
        if (cartDetail && cartDetail.length > 0) {
            cartDetail.map(item => {
                total += parseInt(item.product.product_variant[0].offer_price, 10) * item.items_count
            });
    
            setTotal(total)
            setShowTotal(true)
        }
    }

    const submitOrder = () => {
        
        fetch(`/api/v1/order/buyer=${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                setLoading(false);
                history.push(`/my-addresses/buy/${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}`)
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
            })
    }

    const updateItemQuantity = (cartItemId, requestJson, oldquantity, newquantity, itemprice) => {
        // api/v1/order-detail/id=<uuid:pk>
        fetch(`/api/v1/order-detail/id=${encodeURIComponent(cartItemId)}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
            body: JSON.stringify(requestJson),
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                if (oldquantity > newquantity) {
                    setTotal(Number(Total) - ((Number(oldquantity) - Number(newquantity)) * Number(itemprice)))
                } else {
                    setTotal(Number(Total) + ((Number(newquantity) - Number(oldquantity)) * Number(itemprice)))
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const handleProceedBuySubmit = () => {
        setLoading(true)
        history.push(`/my-addresses/buy/${encodeURIComponent(CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8))}`)
    }

    const removeFromCart = (cartItemId) => {

        fetch(`/api/v1/order-detail/id=${encodeURIComponent(cartItemId)}`, {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            },
        })
            .then((response) => {
                if (response.ok) {
                    return response;
                } else {
                    throw new Error(response);
                }
            })
            .then(function (resp) {
                console.log(resp);
                window.location.reload();
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const renderCartImage = (images) => {
        if (images.length > 0) {
            return `${configData.IMG_FOLDER_PATH}${images[0].image}`
        }
        return `${configData.DEFAULT_PRODUCT_IMG}`
    }

    const renderItemQuantity = (stock, quantityValue, id, productId, cartItemId, itemprice) => {
        
        let quantityArray = [...initialQuantity]
        if (!quantityArray[id]) {
            quantityArray[id] = quantityValue
            setInitialQuantity(quantityArray)
        }
        
        return (<div className="cart-item-Quantity-number-input">
            <NumericInput
                type={'real'}
                step={1}
                min={1}
                max={stock}
                value={quantity[id] ? quantity[id] : quantityValue}
                onChange={e => handleQuantityChange(e, id, productId, cartItemId, itemprice)}
                variant={'secondary'}
                size="sm"
            />
        </div>)
    }

    const handleQuantityChange = (e, id, productId, cartItemId, itemprice) => {
        let newArr = [...quantity]

        let oldquantity = 1
        if (quantity[id]) {
            oldquantity = quantity[id]
        } else {
            oldquantity = initialQuantity[id]
        }
        
        newArr[id] = e
        setquantity(newArr)
        
        const jsonData = {
            buyer: CryptoJS.AES.decrypt(Cookies.get('id'), `${configData.CRYPTO_SECRET_KEY}`).toString(CryptoJS.enc.Utf8),
            product: productId,
            items_count: e,
            total_payable: 0
        };
        
        updateItemQuantity(cartItemId, jsonData, oldquantity, newArr[id], itemprice)
    }

    return (
        <div>
            {isLoading ? <Loader /> :
                <div style={{ width: '85%', margin: '3rem auto' }}>
                    <div>

                        <Container className="container-cartitem">
                            {CartItems && CartItems.length > 0 ?
                                <div>
                                    <div>
                                        <Row>
                                            <Col>
                                                <div className="cart-title">
                                                    Your Shopping Cart Items
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                            </Col>
                                            <Col>
                                            </Col>
                                            <Col>
                                            </Col>
                                            <Col>
                                                <div className="cart-item-header">
                                                    Price per product
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div>
                                        <hr />
                                    </div>
                                </div>
                                : null
                            }
                    
                            {CartItems.map((cartItem, id) => (
                                <div key={id}>
                                    <div>
                                        <Row key={id}>
                                            <Col>
                                                <img alt="product" className="cart-item-img"
                                                    src={renderCartImage(cartItem.product.product_variant[0].product_images)} />
                                            </Col>
                                            <Col>
                                                <div className="cart-item-name-div">
                                                    <a className="cart-item-name" href={`/product/${cartItem.product.id}`}>{cartItem.productName}</a>
                                                </div>
                                                <div>
                                                    {cartItem.productDescription}
                                                </div>
                                                
                                                {cartItem.product.product_variant[0].stock >= cartItem.items_count ?
                                                    <div className="cart-item-instock">Instock</div> :
                                                    <div className="cart-item-outofstock">Out of Stock</div>
                                                }
                                                {cartItem.product.product_variant[0].stock > 0 && cartItem.product.product_variant[0].stock <= 10 ?
                                                    <div className="cart-item-left" >HURRY! Only {cartItem.product.product_variant[0].stock} item(s) left</div>
                                                    : null
                                                }
                                            </Col>
                                            <Col>
                                                <div className="cart-item-quantity-select">
                                                    <div className="cart-item-Quantity">
                                                        {renderItemQuantity(cartItem.product.product_variant[0].stock, cartItem.items_count, id, cartItem.product.id, cartItem.id, cartItem.product.product_variant[0].offer_price)}
                                                    </div>
                                                    <div className="cart-item-remove">
                                                        <Link className="cart-item-remove-link" onClick={() => removeFromCart(cartItem.id)}>Remove</Link>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col>
                                                <div className="cart-item-price">
                                                    &#8377; {cartItem.product.product_variant[0].offer_price}
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div>
                                        <hr />
                                    </div>
                                </div>
                            ))
                            }
                            {ShowTotal ?
                                <Row>
                                    <Col>
                                    </Col>
                                    <Col>
                                    </Col>
                                    <Col>
                                        <div className="cart-item-total">
                                            Total amount:
                                        </div>
                                    </Col>
                                    <Col>
                                        <div className="cart-item-footer">
                                            <div className="cart-item-total-amount">&#8377; {Total} </div>
                                            <div className="cart-item-proceed-btn">
                                                <Button variant="success" onClick={handleProceedBuySubmit}>Place Order</Button>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                :
                                null
                            }
                            <Row>
                                <Col>
                                    {!CartItems || CartItems.length == 0 ?
                              
                                        <div style={{
                                            width: '100%', display: 'flex', flexDirection: 'column',
                                            justifyContent: 'center'
                                        }}>

                                            {Cookies.get('name') == null ?
                                                <div className="cart-empty">
                                                    <div className="cart-empty-header">
                                                        You have not logged in yet!
                                                    </div>
                                                    <div className="cart-empty-login-link-div">
                                                        <Button className="cart-empty-login-link" onClick={handleShow}>Signin here to view your cart items </Button>
                                                    </div>
                                                    <div>
                                                        <SignInModal
                                                            show={show}
                                                            setModalClose={handleClose}
                                                            onHide={handleClose}
                                                        />
                                                    </div>
                                                </div>
                                                :
                                                <div className="cart-empty">
                                                    <div className="cart-empty-header">
                                                        Your Cart is empty
                                                    </div>
                                                    <div className="cart-empty-message">
                                                        You can buy any livestock, livestock accesseries and related products from any where in india through our website in <a className="cart-empty-buy-link" href="/buy/0">here</a>. We take care of your product delivery for areas we are in operational, also we guarantee that the product you buy is of good quality and hygiene
                                                    </div>
                                                </div>
                                            }
                                            
                                        </div>
                                        : null
                                    }
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            }
        </div>
    )
}